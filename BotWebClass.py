import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException, ElementClickInterceptedException
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from datetime import datetime
import os, logging, coloredlogs

MARGIN = 0

class BotWeb:

    def __init__(self, driver_path, headless=False, name="botweb", debug=False, timeout=60):

        # Init vars
        self.timeout = timeout
        self.name = name
        self.bottom_banner_size = MARGIN 
        self.debug = debug

        self.init_log()
        
        # Options
        chrome_options = Options()
        chrome_options.page_load_strategy = 'normal'
        if headless:    
            chrome_options.add_argument("--headless")  # Run in headless mode
            chrome_options.add_argument("--disable-gpu")  # Disable GPU support (recommended for headless)
            self.log_to_file("Headless mode activated")
            self.init_driver(driver_path, chrome_options)
        else:
            self.log_to_file("Headless mode deactivated")

        # Initialize the Chrome driver with the service
        self.init_driver(driver_path, chrome_options)
        self.driver.implicitly_wait(self.timeout)
        self.wait = WebDriverWait(self.driver, self.timeout)
    
    def __del__(self):
        # self.log_to_file(f"{self.name} destroyed")
        self.driver.quit()

    def init_log(self):
        # Create a folder named "logs" if it doesn't exist
        log_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'logs')
        os.makedirs(log_folder, exist_ok=True)
        # Log file base on date
        self.script_dir = os.path.dirname(os.path.abspath(__file__))
        self.log_folder = os.path.join(self.script_dir, log_folder)
        current_datetime = datetime.now()
        date_string = current_datetime.strftime("%Y-%m-%d_%H-%M-%S")
        self.filename_log = os.path.join(self.log_folder, f"log_{self.name}_{date_string}.txt")

        # Create my logger
        self.logger = logging.getLogger(f"{self.name}_logger")

        # Create the file handler, always in debug mode
        file_handler = logging.FileHandler(self.filename_log)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)
        file_handler.setLevel(logging.DEBUG) 
        # Add the file handler to the logger
        self.logger.addHandler(file_handler)
        # Set the global level to DEBUG
        self.logger.setLevel(logging.DEBUG)
        if self.debug:
            coloredlogs.install(level='DEBUG', logger=self.logger)
            self.logger.log(logging.DEBUG, "Log in Dbug mode")
        else:
            coloredlogs.install(level='INFO', logger=self.logger)
            self.logger.log(logging.DEBUG, "Log in INFO mode, Dbug in logfile")

    def get(self,url):
        self.driver.get(url)
        viewport_size = self.driver.execute_script("return { width: window.innerWidth, height: window.innerHeight }")
        self.window_width = viewport_size['width']
        self.window_height = viewport_size['height']
        self.log_to_file(f"Viewport Size - Width: {self.window_width}, Height: {self.window_height}")

    def set_banner(self, xpath=None):
        if xpath != None:
            banner = self.wait.until(EC.presence_of_element_located((By.XPATH, xpath)))
            self.bottom_banner_size = banner.size['height'] + MARGIN
        else:
            self.bottom_banner_size = MARGIN
        self.log_to_file(f"Banner height set to {self.bottom_banner_size}")

    def log_to_file(self, message, level=None):
        if level == None:
            level = logging.DEBUG
        # Open the file in append mode and write the message
        self.logger.log(level, message)

    def init_driver(self, driver_path, options=None):
        # Initialisation de Selenium avec le navigateur Chrome
        driver_service = Service(driver_path)
        if options is None:
            self.driver = webdriver.Chrome(service=driver_service)
        else:
            self.driver = webdriver.Chrome(service=driver_service, options=options)

    def waits_for_xpath(self, xpath, timeout=None):
        if timeout is None:
            timeout = self.timeout
        def callback(driver):
            try:
                # Check if the element is present
                element = self.driver.find_element(By.XPATH, xpath)
                return element is not None and element.is_displayed()
            except:
                return False
        WebDriverWait(self.driver, timeout).until(callback)

    def waits_for_loading(self, element, timeout=None):
        if timeout is None:
            timeout = self.timeout
        def callback(driver):
            # Check if element is xpath or not
            try:
                if element.is_displayed():
                    # Waits for page to be loaded
                    wait = WebDriverWait(driver, timeout)
                    wait.until(EC.presence_of_element_located((By.XPATH, '//*[not(@src) and not(@href)]')))
                    return True
            except Exception as e:
                pass
        WebDriverWait(self.driver, timeout).until(callback)
    

    def wait_and_scroll(self, element, timeout=None):
        self.waits_for_loading(element, timeout)
        def scroll_to_element(element):     
            ActionChains(self.driver).move_to_element(element).perform()
            if self.bottom_banner_size != 0:
                self.driver.execute_script(f"window.scrollBy(0, {self.bottom_banner_size});")
            return True
        
        scroll_to_element(element)

    def wait_and_click(self, element, task_description=None, timeout=None):
        if task_description is not None:
            self.log_to_file(task_description) 
        self.wait_and_scroll(element, timeout)
        if element.is_enabled():
            retries = 2
            while retries > 0:
                try:
                    element.click()
                    break # break while
                except ElementClickInterceptedException as e:
                    time.sleep(1)
                    retries -= 1
                    if self.bottom_banner_size != 0:
                        self.driver.execute_script(f"window.scrollBy(0, {self.bottom_banner_size});")

    def wait_and_click_xpath(self, xpath, task_description=None, timeout=None):
        element = self.find_one_element_by_xpath(xpath, timeout)
        self.wait_and_click(element, task_description, timeout)

    def wait_and_click_xpath_if_exists(self, xpath, task_description=None, timeout=None):
        element = self.find_one_element_by_xpath_if_exists(xpath, timeout)
        if element is not None:
            self.wait_and_click(element, task_description, timeout)
            return True
        return False
        
    def wait_and_fill(self, element, text, task_description=None, timeout=None):
        if timeout is None:
            timeout = self.timeout
        self.wait_and_scroll(element, timeout)
        def wait_and_fill_element(driver):
            if element.is_enabled():
                if task_description is None:
                    self.log_to_file(f"Filling {text}") 
                else:
                    self.log_to_file(task_description) 
                element.send_keys(text)
                return True
            return False
        WebDriverWait(self.driver, timeout).until(wait_and_fill_element)

    def wait_and_select(self, element, index_or_text, task_description=None, timeout=None):
        if timeout is None:
            timeout = self.timeout
        self.wait_and_scroll(element, timeout)

        def wait_and_select_option(driver):
            if element.is_enabled():
                select = Select(element)
                if task_description is None:
                    self.log_to_file(f"Selecting {str(index_or_text)}") 
                else:
                    self.log_to_file(task_description) 
                if isinstance(index_or_text, str):
                    select.select_by_visible_text(index_or_text)
                else:
                    select.select_by_index(index_or_text)
                return True
            return False

        WebDriverWait(self.driver, timeout).until(wait_and_select_option)

    def handles_loading_page(self, xpath, timeout=None):
        if timeout is None:
            timeout = self.timeout
        WebDriverWait(self.driver, timeout).until(EC.invisibility_of_element_located((By.XPATH, xpath)))

    def find_one_element_by_xpath(self, xpath, timeout=None):
        elements = self.find_elements_by_xpath(xpath, timeout)
        if len(elements) != 1:
            self.log_to_file(f"Number of element(s) found using '{xpath}' is incorrect : {len(elements)}", logging.ERROR)
            for element in elements:
                self.log_to_file(f"Element:\n{str(element.get_attribute('outerHTML'))}\n\n", logging.ERROR)
            return None
        else:
            return elements[0]
    
    def find_elements_by_xpath(self, xpath, timeout=None):
        if timeout is not None:
            self.driver.implicitly_wait(float(timeout) / 2)
        else:
            timeout = self.timeout
        self.waits_for_xpath(xpath, float(timeout) / 2)
        elements = self.driver.find_elements(By.XPATH, xpath)
        if len(elements) > 0:
            self.waits_for_loading(elements[0], timeout)
        if timeout is not None:
            self.driver.implicitly_wait(self.timeout)
        return elements
    
    def find_elements_by_xpath_if_exists(self, xpath, timeout=None):
        elements = []
        try:
            elements = self.find_elements_by_xpath(xpath, timeout)
        except Exception as e:
            # self.log_to_file(f"Element {xpath} not found : {str(e)}")
            elements = []
        # In case it fails it may no set it back in find_elements_by_xpath method
        if timeout is not None:
            self.driver.implicitly_wait(self.timeout)
        return elements
    
    def find_one_element_by_xpath_if_exists(self, xpath, timeout=None):
        elements = self.find_elements_by_xpath_if_exists(xpath, timeout)
        if len(elements) > 1:
            self.log_to_file(f"Number of element(s) found using '{xpath}' is incorrect : {len(elements)}", logging.WARNING)
            for element in elements:
                self.log_to_file(f"Element:\n{str(element.get_attribute('outerHTML'))}\n\n", logging.WARNING)
            return elements[0]
        elif len(elements) == 0:
            return None
        else:
            return elements[0]
