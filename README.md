# Bot de Réservation pour le Club de Padel

## Description

Ce script Python automatise le processus de réservation pour le Club de Padel. Il utilise Selenium pour naviguer sur le site web et réserver des terrains de padel à des heures spécifiques.

## Prérequis

Avant d'exécuter le script, assurez-vous de disposer des éléments suivants :

- Python 3 installé sur votre système, testé avec Python 3.11.5. Si ce n'est pas le cas, téléchargez-le sur [le site officiel de Python](https://www.python.org/downloads/).
- Il est plus que conseillé d'utiliser un venv (https://docs.python.org/fr/3/tutorial/venv.html)

## Installation

1. Clonez ce dépôt sur votre machine locale :
```bash
git clone https://gitlab.com/bchaumes/bot_padel.git
```

2. Installez les packages Python requis à l'aide de pip :
```bash
cd bot_padel
pip install -r requirements.txt
```

3. Téléchargez et installez l'exécutable ChromeDriver :

- Rendez-vous sur la page de téléchargement de ChromeDriver (https://chromedriver.chromium.org/downloads).
- Téléchargez la version de ChromeDriver correspondant à votre version de navigateur Chrome.
- Décompressez l'archive téléchargée et placez le fichier chromedriver.exe dans un répertoire.
- Prenez note du chemin d'accès vers le fichier chromedriver.exe, car vous en aurez besoin pour exécuter le script.

## Utilisation

Le script réserve systématiquement 7 jours plus tard.
/!\ Lancer le script entre 23:30 et minuit, dans cette tranche horaire se connectera et attendra minuit pour réserver.
Vous pouvez exécuter le script avec la commande suivante :
```bash
python main.py --driver-path 'chemin_vers_chromedriver' --email 'votre_email' --password 'votre_mot_de_passe' --hours '18:00' '19:30' --headless 'False' --debug
```
Headless permet d'afficher ou non le navigateur. Pour vérifier ce qu'il se passe il vaut mieux mettre 'False' pour économiser des ressources il vaut mieux mettre 'True' lorsqu'on souhaite réserver à heure de pointe.

