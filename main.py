
from BotWebClass import BotWeb
from selenium.webdriver.common.by import By
from datetime import datetime, timedelta
import time, argparse, logging, calendar, random

def wait_until_midnight_if_not_yet_next_day():
    current_time = datetime.now()

    if int(current_time.hour) == 23 and (current_time.minute) > 30:
        # Calculate the time difference until midnight of the next day
        target_time = current_time.replace(hour=0, minute=0, second=1, microsecond=0) + timedelta(days=1)
        time_difference = target_time - current_time
        seconds_to_midnight = time_difference.total_seconds()

        # Wait until midnight
        bot.log_to_file(f"Waiting for {seconds_to_midnight} seconds until midnight and 1 second...")
        time.sleep(seconds_to_midnight)

    bot.log_to_file("It's passed midnight")

# Record the start time
start_time = time.time() 

# RETURNS CODE
NO_SLOTS_FOUND = -10042
DEFAULT_RETURN = -10039
PAYMENT_CARD_SHOULD_BE_ADDED = -10043
RESERVATION_MAY_HAVE_FAILED = -10044
ALREADY_ONE_RESERVATION = -10045
BAD_ARGS = -10040
RESERVATION_SUCCESS = 0

# Init return code
RETURN_CODE = DEFAULT_RETURN

URL = 'https://toulousepadelclub.gestion-sports.com/connexion.php?'
TIMEOUT = 90


parser = argparse.ArgumentParser(description='Automated Booking Script for Padel Club')
parser.add_argument('--driver-path', required=True, help='Path to the driver executable')
parser.add_argument('--email', required=True, help='Your email')
parser.add_argument('--password', required=True, help='Your password')
parser.add_argument('--hours', nargs="+", required=True, help='List of hours separated by space')
parser.add_argument('--headless', required=True, help='Headless or not')
parser.add_argument('--debug', action='store_true', help='Debug mode')

try:
    args = parser.parse_args()
except:
    print("Please provide all required parameters: --driver-path, --email, --password, --hours --headless --debug")
    print("Exemple: python main.py --debug --driver-path='D:/Dev/Projets/bot_padel/chromedriver-win64/chromedriver.exe' --email='my_email' --password='mypass' --hours '18:00' '19:30' --headless='False'")
    exit(BAD_ARGS)

# Replace with your string
headless_bool = args.headless.lower() == "true"

## Initialize Selenium with Chrome
bot = BotWeb(args.driver_path, headless_bool, "bot_padel", args.debug, TIMEOUT)
bot.get(URL)

## Login
bot.wait_and_fill(bot.find_one_element_by_xpath(f'//div[@class="wrap-input100 validate-input form_connexion_input step-1_co show-partner"]//input[contains(@placeholder, "Email")]'), args.email)
bot.wait_and_click_xpath('//button[contains(., "Connexion / Inscription")]')
bot.wait_and_fill(bot.find_one_element_by_xpath(f'//div[@class="wrap-input100 validate-input form_connexion_input password_input step-2_co show-partner"]//input[contains(@placeholder, "Mot de passe")]'), args.password, "Filling password")
bot.wait_and_click_xpath('//button[contains(., "Se connecter")]')

# This is not present each time
bot.log_to_file(f"Try to fill info on level, does not displays each time")
bot.wait_and_click_xpath_if_exists('//*[@id="crea_partie_niveau"]/div/div/div[2]/form/div[3]/button',  "Fill information on level, usually only on first connection", 3)

# Set banner size
bot.set_banner('//div[@class="appBottomMenu"]')
bot.log_to_file(f"Logged successfully as {args.email}", logging.INFO)

# Waits if it is still before 0h
wait_until_midnight_if_not_yet_next_day()

# Clic sur btn rond "Passer" de la pub
bot.log_to_file(f"Skipping ads if exists")
bot.wait_and_click_xpath_if_exists("//a[contains(text(), 'Passer')]")

# Clic sur btn rond "Réserver"
bot.wait_and_click_xpath("//div[@class='globe-principal' and span[text()='Réserver']]")

# Select padel and click on date
bot.wait_and_select(bot.find_one_element_by_xpath('//*[@id="sport"]'), "Padel")
bot.wait_and_click_xpath('//*[@id="date"]')

# Handle date +7d after current day
current_datetime = datetime.now()
current_day, current_month, current_year = current_datetime.strftime('%d'), current_datetime.strftime('%m'), current_datetime.strftime('%Y')
date_voulue = (datetime.now() + timedelta(days=7)).day
wanted_month = (datetime.now() + timedelta(days=7)).month

if date_voulue < int(current_day):
    bot.wait_and_select(bot.find_one_element_by_xpath('//select[@class="ui-datepicker-month"]'), 1, "Selecting the next month")
bot.wait_and_click_xpath(f'//a[@class="ui-state-default" and contains(text(), "{date_voulue}")]', f"Clicking date {date_voulue}")

# Handle hour 
SLOT_FOUND = 0
# for hour in args.hours:
def get_slots_from_hour(hour):
    _SLOT_FOUND = 0
    bot.log_to_file(f"Checking slots for {hour} on {date_voulue}/{wanted_month}", logging.INFO)
    # Before ':', example => 18:00 => 18
    only_hour = hour[:hour.find(":")]
    # Case 8h
    if len(only_hour) == 1:
        only_hour = "0" + only_hour
    bot.wait_and_select(bot.find_one_element_by_xpath('//*[@id="heure"]'), f"{only_hour}:00", f"Selecting '{only_hour}h' for the time slot")

    # Find available slots
    ### Cherche une div de classe card-body contenant une div de classe 'card-title' contenant en texte 'court' et ensuite selectionne le button qui contient le text hour (au format 18:00 ou 19:30)
    xpath_slot = f"//div[@class='card-body' and .//div[@class='card-title' and contains(text(), 'Court')]]//button[contains(text(), '{hour}')]"
    _slots = bot.find_elements_by_xpath_if_exists(xpath_slot, 90)
    if len(_slots) > 0:
        retries = 0  # Set the cpt of retry attempts
        bot.log_to_file(f"{str(len(_slots))} slots availables for {hour} on {date_voulue}/{wanted_month}", logging.INFO)
        while retries <= 2:
            try:
                # If click fails, refresh
                if retries != 0:
                    # Switch hour to refresh
                    bot.wait_and_select(bot.find_one_element_by_xpath('//*[@id="heure"]'), f"07:00", f"Selecting '07:00' for the time slot in order to refresh")
                    bot.wait_and_select(bot.find_one_element_by_xpath('//*[@id="heure"]'), f"{only_hour}:00", f"Selecting '{only_hour}h' for the time slot")
                    time.sleep(2)
                    _slots = bot.find_elements_by_xpath_if_exists(xpath_slot)
                # Try to take the 3rd slots if more than 3 available otherwise take the last available
                rand_nslot = random.randint(0, min(8, len(_slots)) - 1)
                bot.log_to_file(f"Slot number {str(rand_nslot)} has been select randomly (len(slots) = {str(len(_slots))})")
                _selected_slot = _slots[rand_nslot]
                bot.wait_and_click(_selected_slot, f"Clicking time slot {hour}")
                _SLOT_FOUND = 1
                break  # Exit the while loop if the click is successful
            except Exception as e:
                # Add a delay if needed
                time.sleep(2)  # Adjust the sleep time as needed
                retries += 1
                continue  # Retry the click
        # break # breaks for loop
        return _SLOT_FOUND, _slots, _selected_slot
    else:
        bot.log_to_file(f"No time slot for {hour} on {date_voulue}/{wanted_month}", logging.INFO)
        try:
            bot.log_to_file(f"Try to log slots displayed if any")
            courts = bot.find_elements_by_xpath_if_exists("//div[@class='card-body' and .//div[@class='card-title' and contains(text(), 'Court')]]", 10)
            if len(courts):
                all_slots_str = "\n"
                for court in courts:
                    bouttons_court = court.find_elements(By.XPATH, ".//button")
                    # button_text_concatenated = ', '.join([button.text for button in bouttons_court])
                    return_line = "\n"
                    all_slots_str += court.text.replace("\n", ' ') + '\n'
                bot.log_to_file(all_slots_str)                
        except Exception as e:
            pass
        _SLOT_FOUND = 0
        return _SLOT_FOUND, None, None


def reserve_from_slot(_slot):
    # Get court name and clic on the corresponding Réserver btn
    card_title_element = _slot.find_element(By.XPATH, "./ancestor::div[@class='card-body']//div[@class='card-title']")
    _court_name = card_title_element.text
    bot.log_to_file(f"Reserving {_court_name} for {hour} on {date_voulue}/{wanted_month}", logging.INFO)
    card_body_element = _slot.find_element(By.XPATH, "./ancestor::div[@class='card-body']")
    btn_reserver_from_same_body = card_body_element.find_element(By.XPATH, ".//button[.//span[contains(text(), 'Réserver')]]")
    bot.wait_and_click(btn_reserver_from_same_body)
    # Click on 'Payer sur place'
    retries = 5
    return_click = False
    while retries > 0 and not return_click : # handles bug of modal-footer which intercept
        # try:
        return_click = bot.wait_and_click_xpath_if_exists("//a[contains(@style, 'opacity: 1') and .//span[text()='Payer sur place']]")
        if return_click:
            break
        # except Exception as e:
        else:
            bot.log_to_file(f"Failed to click on 'Payer sur place', retrying...", logging.WARNING)
            return_click = bot.wait_and_click_xpath_if_exists('//*[@id="choix_paiement"]/div/div/div[4]/div/a[3]', "Clicking 'Payer sur place', hardcoded mode")
            time.sleep(2)
            retries -= 1
    retries = 2
    while retries > 0: 
        try:
            confirm_button = bot.find_elements_by_xpath_if_exists("//a[contains(., 'Confirmer')]")
            if len(confirm_button) > 0:
                bot.log_to_file("Find confirmer button")
                bot.wait_and_click(confirm_button[0], "Clicking 'Confirmer'")
                break
            else:
                bot.log_to_file("Did not find confirmer button, try hardcoded mode", logging.WARNING)
                return_click = bot.wait_and_click_xpath_if_exists('//*[@id="choix_paiement"]/div/div/div[4]/div/a[3]', "Clicking 'Confirmer', hardcoded mode", 10)
                retries -= 1
        except Exception as e:
            bot.log_to_file(f"Failed to click on 'Confirmer button' : \n{e}", logging.WARNING)
            time.sleep(2)
            retries += 1

    return _court_name

for hour in args.hours:
    SLOT_FOUND, slots, selected_slot = get_slots_from_hour(hour)
    if SLOT_FOUND == 1 and slots != None and selected_slot != None and len(slots) > 0:
        court_name = reserve_from_slot(selected_slot)
        # Check if slots already taken faster by someone
        # elements = bot.find_elements_by_xpath_if_exists("//div[@class = 'notification-content' and .//div[contains(text(), 'plus disponible')]]", 10)
        elements = bot.find_elements_by_xpath_if_exists("//div[@class = 'notification-content']//div[@class = 'in']", 20)
        success = bot.find_elements_by_xpath_if_exists('//p[contains(., "votre réservation s\'est bien effectuée")]', 10)
        if len(elements) > 0 and len(success) == 0:
            div_text = elements[0].find_element(By.XPATH, ".//div[@class = 'text']").text
            bot.log_to_file(f"Notification popped up, something is wrong:\n{div_text}", logging.WARNING)
            if "Désolé mais vous avez 1 réservation en cours" in div_text:
                bot.log_to_file("This account has aleady a reservation booked, only one by account is authorized", logging.ERROR)
                # Record the end time
                end_time = time.time()
                # Calculate the total time
                total_time = end_time - start_time
                bot.log_to_file(f"Total time taken: {total_time:.2f} seconds", logging.INFO)
                exit(ALREADY_ONE_RESERVATION)
            else: # "plus disponible" in div_text:
                bot.log_to_file("Slot not available anymore, need to try another", logging.WARNING)
                for backward_slot in reversed(slots):
                    try:
                        bot.wait_and_click(backward_slot)
                        court_name = reserve_from_slot(backward_slot)
                    except Exception as e:
                        pass
                    # Test if reservation confirmed
                    element = bot.find_one_element_by_xpath_if_exists('//p[contains(., "votre réservation s\'est bien effectuée")]', 30)
                    if element is not None:
                        bot.log_to_file(f"Réservation successfull {court_name} for {hour} on {date_voulue}/{wanted_month}", logging.INFO)
                        RETURN_CODE = RESERVATION_SUCCESS
                        break # breaks for loop for backward_slot in reversed(slots):
                    else:
                        bot.log_to_file("Backward reserving failed, continue", logging.WARNING)
                if RETURN_CODE == RESERVATION_SUCCESS:
                    break # # breaks for hour in args.hours:
                
        else:

            # Test if reservation confirmed
            element = bot.find_one_element_by_xpath_if_exists('//p[contains(., "votre réservation s\'est bien effectuée")]', 20)
            if element is not None:
                bot.log_to_file(f"Réservation successfull {court_name} for {hour} on {date_voulue}/{wanted_month}", logging.INFO)
                RETURN_CODE = RESERVATION_SUCCESS
                break # breaks for hour in args.hours:
            else:
                bot.log_to_file(f"It may have been a problem with the reservation of {court_name} for {hour} on {date_voulue}/{wanted_month}", logging.WARNING)
                RETURN_CODE = RESERVATION_MAY_HAVE_FAILED
                # Check if paiement needs to be added
                elements = bot.find_elements_by_xpath_if_exists("//h5[contains(., 'Ajouter un moyen de paiement')]", 10)
                if len(elements) != 0:
                    bot.log_to_file("Paiement needs to be added to the account", logging.ERROR)
                    RETURN_CODE = PAYMENT_CARD_SHOULD_BE_ADDED

if SLOT_FOUND == 0:
    bot.log_to_file(f"No slot available for {str(args.hours)}", logging.ERROR)
    # Record the end time
    end_time = time.time()
    # Calculate the total time
    total_time = end_time - start_time
    bot.log_to_file(f"Total time taken: {total_time:.2f} seconds", logging.INFO)
    exit(NO_SLOTS_FOUND)

# Record the end time
end_time = time.time()
# Calculate the total time
total_time = end_time - start_time
bot.log_to_file(f"Total time taken: {total_time:.2f} seconds", logging.INFO)

exit(RETURN_CODE)
